# -*- coding: utf-8 -*-
# Copyright (C) 2014 Hennecke Baptiste, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import CombinaisonSpacialOnEvent,CombinaisonSpacialOffEvent

class (Action2):
    EVENT = CombinaisonSpacialOnEvent
    ACTION = "dress-up"
    RESOLVE_OBJECT = "resolve_for_use"

class (Action2):
    EVENT = CombinaisonSpacialOffEvent
    ACTION = "removed"
    RESOLVE_OBJECT = "resolve_for_use"
