# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class CombinaisonOnEvent(Event2):
    NAME = "dress-up"

    def perform(self):
        if not self.object.has_prop("lightable"):
            self.fail()
            return self.inform("dress-up.failed")
        self.inform("dress-up")


class CombinaisonOffEvent(Event2):
    NAME = "removed"

    def perform(self):
        if not self.object.has_prop("lightable"):
            self.fail()
            return self.inform("removed.failed")
        self.inform("removed")
