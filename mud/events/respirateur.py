# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class RespirateurOnEvent(Event2):
    NAME = "respirateur-on"

    def perform(self):
        if not self.object.has_prop("respirer"):
            self.fail()
            return self.inform("respirateur-on.failed")
        self.inform("respirateur-on")


class RespirateurOffEvent(Event2):
    NAME = "respirateur-off"

    def perform(self):
        if not self.object.has_prop("respirer"):
            self.fail()
            return self.inform("respirateur-off.failed")
        self.inform("respirateur-off")
